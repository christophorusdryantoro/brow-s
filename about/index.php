<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Lewat BROW-S, kamu bisa meminjam dan meminjamkan barang ke orang lain dengan aman, nyaman, dan cepat. BROW-S, kalau bisa minjem, ngapain beli?">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta name="author" content="Christophorus Dryantoro">
  <meta name="image" content="../img/Brow-S.png">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>BROWS - Platform Peminjaman Barang Online Teraman dan Terlengkap</title>
	<link rel="icon" href="../img/thumbnail.png" type="image/png">
  <link rel="stylesheet" href="../vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="../vendors/fontawesome/css/all.min.css">
	<link rel="stylesheet" href="../vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="../vendors/nice-select/nice-select.css">
  <link rel="stylesheet" href="../vendors/owl-carousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="../vendors/owl-carousel/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

  <link rel="stylesheet" href="../css/style.css?cf=201909191413">
  
  <style>
  	#skrollr-body {
    float: left !important;
    width: 100% !important;
    height: 100% !important;
}
  </style>
</head>
<body>
  <!--================ Start Header Menu Area =================-->
	<header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
          <a class="navbar-brand logo_h" href="../index.php"><img src="../img/Brow-S.png" height="40px" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav ml-auto mr-auto">
              <li class="nav-item"><a class="nav-link" href="../index.php">Home</a></li>
              <li class="nav-item submenu dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                  aria-expanded="false">Rent</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a class="nav-link" href="../category.html">Product Category</a></li>
                  <li class="nav-item"><a class="nav-link" href="../single-product.html">Product Details</a></li>
                  <li class="nav-item"><a class="nav-link" href="../checkout.html">Product Checkout</a></li>
                  <li class="nav-item"><a class="nav-link" href="../confirmation.html">Confirmation</a></li>
                  <li class="nav-item"><a class="nav-link" href="../cart.html">Shopping Cart</a></li>
                </ul>
							</li>
			<li class="nav-item submenu dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                  aria-expanded="false">Account</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a class="nav-link" href="../login.html">Login</a></li>
                  <li class="nav-item"><a class="nav-link" href="../register.html">Register</a></li>
                  <li class="nav-item"><a class="nav-link" href="../tracking-order.html">Tracking</a></li>
                </ul>
              </li>
              <li class="nav-item"><a class="nav-link" href="../contact.html">Contact</a></li>
              <li class="nav-item active"><a class="nav-link" href="../about">About</a></li>
            </ul>

            <ul class="nav-shop">
              <li class="nav-item"><button><i class="ti-search"></i></button></li>
              <li class="nav-item"><button><i class="ti-shopping-cart"></i><span class="nav-shop__circle">3</span></button> </li>
              <li class="nav-item"><a class="button button-header" href="#">BROW-S Now</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>
	<!--================ End Header Menu Area =================-->
	  <main class="site-main">
         	<div id="ajSlider"> 
              <img src="../img/carousel/banner1.png" width="100%" max-height="100%" style="width:100% !important; max-height:100% !important;">
              <img src="../img/carousel/banner2.png" width="100%" max-height="100%" style="width:100% !important; max-height:100% !important;">
              <img src="../img/carousel/banner3.png" width="100%" max-height="100%" style="width:100% !important; max-height:100% !important;">
              <img src="../img/carousel/banner4.png" width="100%" max-height="100%" style="width:100% !important; max-height:100% !important;">
              <img src="../img/carousel/banner5.png" width="100%" max-height="100%" style="width:100% !important; max-height:100% !important;">
             
              <a></a> 
              <a></a> 
              <a></a> 
              <a></a> 
              <a></a> 
            </div> 
	  </main>
	  
	  	<div class="product_image_area">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-6">
					<div class="owl-carousel owl-theme s_Product_carousel">
						<div class="single-prd-item">
							<img class="img-fluid img-landing" src="../img/landingpage/hand.png" height="400px" alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<div class="s_product_text">
						<h1>Easy Registration</h1>
						<br />
						<h3>Registrasi Mudah dan Nggak Pake Ribet</h3>
					</div>
				</div>
			</div>
		</div>
		</div>
		
	  	<div class="product_image_area bg-lightgrey2 padb-lg">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-5 offset-lg-1">
					<div class="s_product_text">
						<h1>Easy Delivery and Return</h1>
						<br />
						<h3>Barang yang kamu sewa langsung dikirim ke alamatmu. Balikinnya? Gampang kok!</h3>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="owl-carousel owl-theme s_Product_carousel">
						<div class="single-prd-item">
							<img class="img-fluid img-landing" src="../img/landingpage/delivery.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		
		<div class="product_image_area padb-lg">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-6 offset-lg-1">
					<div class="owl-carousel owl-theme s_Product_carousel">
						<div class="single-prd-item">
							<img class="img-fluid img-landing" src="../img/landingpage/shield.png" alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="s_product_text">
						<h1>Warranty</h1>
						<br />
						<h3>Barang yang kamu sewakan di BROW-S dijamin aman</h3>
					</div>
				</div>
			</div>
		</div>
		</div>
		
	  	<div class="product_image_area bg-lightblue padb-lg">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-5">
					<div class="s_product_text">
						<h1>Karma System</h1>
						<br />
						<h3>Nggak perlu takut sewa barang di BROW-S, kamu bisa lihat review pemilik barang</h3>
					</div>
				</div>
				<div class="col-lg-6 offset-lg-1">
					<div class="owl-carousel owl-theme s_Product_carousel">
						<div class="single-prd-item">
							<img class="img-fluid img-landing" src="../img/landingpage/review.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		
		<div class="product_image_area padb-lg">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-6 offset-lg-1">
					<div class="owl-carousel owl-theme s_Product_carousel">
						<div class="single-prd-item">
							<img class="img-fluid img-landing" src="../img/landingpage/payment.png" alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="s_product_text">
						<h1>Easy Payment</h1>
						<br />
						<h3>Kamu bebas pilih metode pembayaran yang kamu mau</h3>
					</div>
				</div>
			</div>
		</div>
		</div>
		
		<div class="product_image_area bg-lightgrey padb-lg">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-5">
					<div class="s_product_text">
						<h1>Strong Security</h1>
						<br />
						<h3>Kamu nggak usah khawatir akunmu dibobol, karena BROW-S menggunakan Multi Factor Authentication</h3>
					</div>
				</div>
				<div class="col-lg-6 offset-lg-1">
					<div class="owl-carousel owl-theme s_Product_carousel">
						<div class="single-prd-item">
							<img class="img-fluid img-landing" src="../img/landingpage/locked.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		
		
	<section class="subscribe-position subscribe-custom">
      <div class="container">
      	<div class="subscribe text-center nopad-b">
      	<div class="row">
          <div class="col-lg-5 col-md-5 col-sm-5">
          	<img src="../img/landingpage/Mockup1.png" style="max-height: 250px" />
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5">
              <h3 class="subscribe__title">Kalau Bisa Minjem, <br />Ngapain Beli</h3>
    			<img class="img-download" src="../img/landingpage/Download-On-the-App-store.png" />
    			<img class="img-download" src="../img/landingpage/download-google-play.png" />
          </div>
        </div>
        </div>
      </div>
    </section>
    <!-- ================ Subscribe section end ================= --> 
</body>
<footer class="footer">
		<div class="footer-area">
			<div class="container">
				<div class="row section_gap">
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="single-footer-widget tp_widgets">
							<h4 class="footer_title large_title">Kenapa BROW-S?</h4>
							<p>
								Lewat BROW-S, millenials tidak perlu boros lagi. Mau bergaya, liburan ke luar negeri, pakai accessories mahal, foto pakai drone & lensa mahal, NGGAK perlu BELI lagi.   
							</p>
							<p>
								BROW-S. Kalau bisa minjem, ngapain beli?
							</p>
						</div>
					</div>
					<div class="offset-lg-1 col-lg-2 col-md-6 col-sm-6">
						<div class="single-footer-widget tp_widgets">
							<h4 class="footer_title">Quick Links</h4>
							<ul class="list">
								<li><a href="#">Home</a></li>
								<li><a href="#">Shop</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Product</a></li>
								<li><a href="#">Brand</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-2 col-md-6 col-sm-6">
						<div class="single-footer-widget instafeed">
							<h4 class="footer_title">Gallery</h4>
							<ul class="list instafeed d-flex flex-wrap">
								<li><img src="../img/gallery/r1.jpg" alt=""></li>
								<li><img src="../img/gallery/r2.jpg" alt=""></li>
								<li><img src="../img/gallery/r3.jpg" alt=""></li>
								<li><img src="../img/gallery/r5.jpg" alt=""></li>
								<li><img src="../img/gallery/r7.jpg" alt=""></li>
								<li><img src="../img/gallery/r8.jpg" alt=""></li>
							</ul>
						</div>
					</div>
					<div class="offset-lg-1 col-lg-3 col-md-6 col-sm-6">
						<div class="single-footer-widget tp_widgets">
							<h4 class="footer_title">Supported By</h4>
							<div>
								<img src="../img/telkom-logo-baru-white.png" width="250px"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row d-flex">
					<p class="col-lg-12 footer-text text-center">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This website is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="http://brows.id" target="_blank">BROW-S</a> team
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				</div>
			</div>
		</div>
	</footer>
	<!--================ End footer Area  =================-->


  <script src="../vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="../js/ajSlider.js"></script>
  <script src="../vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="../vendors/skrollr.min.js"></script>
  <script src="../vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="../vendors/nice-select/jquery.nice-select.min.js"></script>
  <script src="../vendors/jquery.ajaxchimp.min.js"></script>
  <script src="../vendors/mail-script.js"></script>
  <script src="../js/main.js"></script>
  <script>
  $(document).ready(function() {
	  var width = $("#ajSlider").width();
	  $("#ajSlider").height(width/1366*500);
	  $("#ajSlider img").height(width/1366*500);
	  $('#ajSlider').ajSlider(3500,{
		   "slideshow":"enable", //disable or enable the slideshow 
		   "width":"100%", //width of slider    
		   "height":"100%", //height of slider  
		   "activeBullet":"lightblue", //color of the active bullet   
		   "inactiveBullet":"white", //color of inactive bullet    
		   "textPosition":"30%", //position of text from top    
		   "textSize":"60px" //font size of the text   
	  });    
	}); 
  </script>
</body>